import axios from 'axios'

export async function getClientIP(){
    const IP_API = process.env.NEXT_PUBLIC_IP_API;

    let ip;
    await axios.get(IP_API)
        .then((results) => {
            ip = results.data.query
        })

    return ip;
}