export const sweaters_data = [
    {
        "id": 1,
        "name": "sweater_1",
        "thumbnail": require("../../public/sweater1.png"),
        "folded": require("../../public/sweater-blue.png")
    },
    {
        "id": 2,
        "name": "sweater_2",
        "thumbnail": require("../../public/sweater2.png"),
        "folded": require("../../public/sweater-darkgreen.png")
    },
    {
        "id": 3,
        "name": "sweater_3",
        "thumbnail": require("../../public/sweater3.png"),
        "folded": require("../../public/sweater-beige.png")
    },
    {
        "id": 4,
        "name": "sweater_4",
        "thumbnail": require("../../public/sweater4.png"),
        "folded": require("../../public/sweater-red.png")
    },
    {
        "id": 5,
        "name": "sweater_5",
        "thumbnail": require("../../public/sweater5.png"),
        "folded": require("../../public/sweater-green.png")
    },
    {
        "id": 6,
        "name": "sweater_6",
        "thumbnail": require("../../public/sweater6.png"),
        "folded": require("../../public/sweater-white.png")
    },
    {
        "id": 7,
        "name": "sweater_7",
        "thumbnail": require("../../public/sweater7.png"),
        "folded": require("../../public/sweater-beige.png")
    },
    {
        "id": 8,
        "name": "sweater_8",
        "thumbnail": require("../../public/sweater8.png"),
        "folded": require("../../public/sweater-lightgreen.png")
    },
    {
        "id": 9,
        "name": "sweater_9",
        "thumbnail": require("../../public/sweater9.png"),
        "folded": require("../../public/sweater-maroon.png")
    },
    {
        "id": 10,
        "name": "sweater_10",
        "thumbnail": require("../../public/sweater10.png"),
        "folded": require("../../public/sweater-green2.png")
    },
    {
        "id": 11,
        "name": "sweater_11",
        "thumbnail": require("../../public/sweater11.png"),
        "folded": require("../../public/sweater-blue.png")
    },
    {
        "id": 12,
        "name": "sweater_12",
        "thumbnail": require("../../public/sweater12.png"),
        "folded": require("../../public/sweater-lightblue.png")
    }
]