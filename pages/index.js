import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import styles from '../styles/modules/Home.module.scss'
import Head from 'next/head'
import Banner from './components/Banner'
import Main from './components/Main'

export default function Home() {
  return (
    <DndProvider backend={HTML5Backend}>
      <div className={styles.container}>
        <Head>
          <title>Nexon</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={styles.main}>
          <Banner />
          <Main />
        </main>

      </div>
    </DndProvider>
  )
}
