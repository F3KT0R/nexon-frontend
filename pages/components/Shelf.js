import React, { useContext, useState } from 'react'
import Image from 'next/image'
import { useDrop } from 'react-dnd'
import styles from '../../styles/modules/Shelf.module.scss'
import { ShelfContext } from './Main'
import { ItemTypes } from '../../utils/items'

function Shelf({ array, link, name }) {

  const numberOfSweaters = (array) =>{
    let counter = 0
    array.map(item => item.name === name ? 
      counter++
      : ''
    )

    return counter
  }

  const { dropOnShelf } = useContext(ShelfContext);
  
  const [{ isOver }, drop] = useDrop({
    accept: ItemTypes.SWEATER,
    drop: (item, monitor) => dropOnShelf(item.id, name),
    collect: (monitor) => ({
      isOver: !!monitor.isOver(),
    }),
  });

  return (
    <div className={styles.container} >
        <div style={{ 
          padding: '.5em',
          display: 'flex',
          flexDirection: 'column-reverse',
          justifyContent: 'center',
          alignItems: 'center'
          }}>
          {array.map((sweater) => {
            return name === sweater.name ?
              <Image key={sweater.id} className={styles.image} src={sweater.folded} alt={sweater.id} />
              : ''
          })}</div>
        <Image
            ref={drop}
            src={link}
            className={styles.shelf}
            alt={name}
        />
        <h2 id={styles.name}>{name}</h2>
        <h2 id={styles.name}>{numberOfSweaters(array)}</h2>
    </div>
  )
}

export default Shelf