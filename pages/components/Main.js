import React, { createContext, useState } from 'react'
import shelf_image from '../../public/shelf.png'
import styles from '../../styles/modules/Main.module.scss'
import Sweater from './Sweater'
import Shelf from './Shelf'
import { sweaters_data } from '../data/sweaters-data'
import { foundations_data } from '../data/foundations-data'
import axios from 'axios'
import { getCurrentDate } from '../../utils/date'
import { getClientIP } from '../../utils/ip'

export const ShelfContext = createContext({
  dropOnShelf: null
});

const URL = 'https://sheet.best/api/sheets/';
const API_KEY = process.env.NEXT_PUBLIC_NEXON_API_KEY;

function Main() {
  const [sweaters, setSweaters] = useState(sweaters_data)
  const [sweatersOnTheShelf, setSweatersOnTheShelf] = useState([])
  const [foundations, setFoundations] = useState(foundations_data);
  const [disabledReset, setDisabledReset] = useState(true)
  const [disabledSend, setDisabledSend] = useState(true)

  const dropOnShelf = (id, name) => {
    const selectedSweater = sweaters.find((picture) => id === picture.id);
    selectedSweater.name = name;
    setSweatersOnTheShelf((sweaters) => [...sweaters, selectedSweater]);
    disabledReset === true ? setDisabledReset(!disabledReset) : ''
    sweaters.map((sweater) => {
      if(sweater.id === selectedSweater.id) {
        const index = sweaters.findIndex(item => item.id === selectedSweater.id);
        sweaters.splice(index, 1)
        setSweaters(sweaters)
        if(sweaters.length === 0) {
          setDisabledSend(!disabledSend)
        }
      }
      return
    })
  };

  const handleReset = () => {
    sweatersOnTheShelf.map((sweater) => {
      setSweaters((sweaters) => [...sweaters, sweater])
    })
    setSweatersOnTheShelf([])
    setDisabledReset(!disabledReset)
    setDisabledSend(true)
    setFoundations(foundations_data)
  }

  const handleFoundationCount = async() =>{
    sweatersOnTheShelf.map((sweater) => {
      foundations.map((foundation) => {
        sweater.name === foundation.name ? foundation.count++ : ''
      })
    })

    const previous_data = await axios.get(URL+API_KEY)
      .then(response => response.data)

    const previous_ip = [];
    previous_data.map((item) => {
      previous_ip.push(item['Client IP'])
    })

    const previous_date_time = [];
    previous_data.map((item) => {
      previous_date_time.push(item['Date & Time'])
    })

    const date_time = getCurrentDate()

    const client_ip = await getClientIP();
    
    
    for(let element in previous_ip) {
      if(previous_ip[element] === null) continue
      if(previous_ip[element].toString() !== client_ip.toString()) continue

      let current_date_time_converted = new Date(date_time).valueOf()
      let previous_date_time_converted = new Date(previous_date_time[element]).valueOf()

      if(current_date_time_converted === 0 || current_date_time_converted === 0 && previous_date_time_converted === null || previous_date_time_converted === 0 ) continue
      if(current_date_time_converted >= previous_date_time_converted + 600000) continue

      alert('Még nem múlt el 10 perc az előző beküldése óta.\nKérjük próbálkozzon később!')
      return 
    }
    
    const data = {
      [foundations[0].name]: foundations[0].count,
      [foundations[1].name]: foundations[1].count,
      [foundations[2].name]: foundations[2].count,
      [foundations[3].name]: foundations[3].count,
      'Client IP': client_ip,
      'Date & Time': date_time
    }

    await axios.post(URL + API_KEY, data)
      .then((res) => console.log(res))
      .catch((error) => {
        if (error.response) {
          console.log(error.response);
          console.log("server responded");
        } else if (error.request) {
          console.log("network error");
        } else {
          console.log(error);
        }
      });
  }

  const handleSend = () => {
    alert('Sikeresen elküldte a döntését!')

    handleFoundationCount()
    setDisabledSend(true)
    setDisabledReset(true)
  }

  return (
    <ShelfContext.Provider value={{dropOnShelf}}>
      <div className={styles.container}>
        <div className={styles.sweater_wrapper}>
          {sweaters.map(({id, thumbnail, name}) => {
            return <Sweater key={id} id={id} link={thumbnail} name={name}/>
          })}
        </div>
        <div id={styles.buttons}>
          <button id={styles.reset} disabled={disabledReset} onClick={handleReset} style={{ display: disabledReset ? 'none' : 'flex' }}>
            Visszaállít
          </button>
          <button id={styles.send} disabled={disabledSend} onClick={handleSend} style={{ display: disabledSend ? 'none' : 'flex' }}>
            Elküld
          </button>
        </div>
        <div className={styles.shelf_wrapper}>
          {foundations.map(({name}) => {
            return <Shelf key={name} array={sweatersOnTheShelf} link={shelf_image} name={name} />
          })}
        </div>

      </div>
    </ShelfContext.Provider>
  )
}

export default Main