import React from 'react';
import Image from 'next/image';
import styles from '../../styles/modules/Sweater.module.scss';
import { useDrag } from "react-dnd";
import { ItemTypes } from '../../utils/items';

function Sweater({ id, link, name }) {

  const [{ isDragging }, drag] = useDrag({
    type: ItemTypes.SWEATER,
    item: { 
      id: id, 
      type: ItemTypes.SWEATER,
    },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  });

  return (
    <div className={styles.container}>
        <Image ref={drag} src={link} alt={name} className={styles.sweater} style={{ opacity: isDragging ? ".5" : "1" }}/>
    </div>
  )
}

export default Sweater